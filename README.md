# leanring-react-native


## Getting started

### INITIAL PROJECT
CMD: expo init

### Start app
``` run npm start```

### Running app on android emulator
- download and install android studio https://developer.android.com/studio
- open android studio, you should see welcome panel, click on symbol : next to get from vcs button then select SDK manager
- in Android SDK menu, verify and check if Android OS is installed in SDK Platforms tab
- verify and check if android emulator, Android SDK Platform tool is installed in SDK Tools tab, intel x86 emulator accelerator and google play services
- click ok
- click ADV manager, create device
- click play button to start emulator
- press a on expo start command prompt to start expo on android emulator
