import React, {useState} from 'react';
import {Button, FlatList, StyleSheet, View} from 'react-native';
import GoalItem from "./components/GoalItem";
import GoalInput from "./components/GoalInput";

export default function App() {
    const [courseGoals, setCourseGoals] = useState([]);
    const [isAddMode, setIsAddMode] = useState(false);

    const addGoalHandler = (enteredGoal: string) => {
        // this way yoo won't guarantee hundred percent state
        // setCourseGoals([...courseGoals, enteredGoal]);
        // this way guarantee latest state
        // @ts-ignore
        setCourseGoals(() => [...courseGoals,
            {
                // add key prop for flatlist to render with unique key
                // it should be unique but this way is fine for now
                key: Math.random().toString(),
                value: enteredGoal
            }]);
        // update 2 state in 1 fn, react only render 1 time, not n state times
        setIsAddMode(false);
    }

    const removeGoalHandler = (goalKey: string) => {
        // @ts-ignore
        setCourseGoals((currentCourseGoal) => {
            return currentCourseGoal.filter((goal: any) => goal.key !== goalKey)
        });
    };

    const cancelAddedGoal = () => {
        setIsAddMode(false);
    }

    return (
        // view is flex box by default, vertical direction (column)
        // justifyContent controls element along main, alignItems controls element along cross axis
        // default view take as much width as its need
        // add flex 1 to child to take available space
        <View style={styles.screen}>
            <Button title={'Add New Goal'} onPress={() => setIsAddMode(true)} />
            <GoalInput visible={isAddMode}
                       onCancel={cancelAddedGoal}
                       onAddGoal={addGoalHandler}/>

            {/*scrollView render content in advance even though we can't see them*/}
            {/*<ScrollView>*/}
            {/*    {courseGoals.map((goal) =>*/}
            {/*        <View style={styles.listItem}><Text key={goal}>{goal}</Text></View>*/}
            {/*    )}*/}
            {/*</ScrollView>*/}
            <FlatList data={courseGoals}
                      // extract key
                      keyExtractor={(item: any) => item.key}
                      renderItem={(itemData: any) =>
                          <GoalItem id={itemData.item.key}
                                    title={itemData.item.value}
                                    onDelete={removeGoalHandler} />
                      } />
        </View>
    );
}

// stylesheet create may opt performance in future and also add some validation
const styles = StyleSheet.create({
    screen: {
        padding: 50
    }
});
