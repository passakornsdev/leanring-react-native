import {Button, Modal, StyleSheet, TextInput, View} from "react-native";
import React, {useState} from "react";

const GoalInput = (props: any) => {
    const [enteredGoal, setEnteredGoal] = useState('');

    const goalInputHandler = (enteredText: string) => {
        setEnteredGoal(enteredText);
    }

    const addGoalHandler = () => {
        props.onAddGoal(enteredGoal);
        setEnteredGoal('');
    }

    return (
        <Modal visible={props.visible} animationType={'slide'}>
            <View style={styles.inputContainer}>
                <TextInput placeholder={'Course Goal'}
                    // dont add () cause we don't want to exec in the first render, without () means reference to fn
                           onChangeText={goalInputHandler}
                    // pass value back to display value
                           value={enteredGoal}
                           style={styles.input}/>
                <View style={styles.buttonContainer}>
                    <View style={styles.button}>
                        <Button title={'CANCEL'}
                                color={'red'}
                                onPress={props.onCancel} />
                    </View>
                    <View style={styles.button}>
                        <Button title={'ADD'}
                            // we can use anonymous function
                            // or use bind to preconfigure parameter
                                onPress={addGoalHandler}/>
                    </View>
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    inputContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        padding: 10,
        width: '80%',
        marginBottom: 10
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '60%'
    },
    button: {
        width: '40%'
    }
});

export default GoalInput;
